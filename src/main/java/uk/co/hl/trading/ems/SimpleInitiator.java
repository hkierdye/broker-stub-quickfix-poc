package uk.co.hl.trading.ems;

import quickfix.Application;
import quickfix.FieldNotFound;
import quickfix.IncorrectTagValue;
import quickfix.Message;
import quickfix.MessageCracker;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.StringField;
import quickfix.UnsupportedMessageType;
import quickfix.field.Currency;
import quickfix.field.FutSettDate;
import quickfix.field.HandlInst;
import quickfix.field.IDSource;
import quickfix.field.OrdType;
import quickfix.field.OrderQty;
import quickfix.field.Price;
import quickfix.field.QuoteReqID;
import quickfix.field.SecurityExchange;
import quickfix.field.SecurityID;
import quickfix.field.SettlCurrency;
import quickfix.field.SettlDate;
import quickfix.field.SettlType;
import quickfix.field.Side;
import quickfix.field.Symbol;
import quickfix.field.TimeInForce;
import quickfix.field.TransactTime;
import quickfix.fix44.QuoteRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SimpleInitiator extends MessageCracker implements Application {

    Session session = null;

    @Override
    public void fromApp(Message message, SessionID sessionId) throws FieldNotFound, IncorrectTagValue, UnsupportedMessageType {
        System.out.println("IN - Message : " + message + " for sessionId : " + sessionId);
        crack(message, sessionId);
    }

    @Override
    public void toApp(Message message, SessionID sessionId) {
        System.out.println("OUT - Message: " + message + " for sessionId: " + sessionId);
    }

    @Override
    public void fromAdmin(Message message, SessionID sessionId) {
        System.out.println("ADMIN IN - Message: " + message + " for sessionId: " + sessionId);
    }

    @Override
    public void toAdmin(Message message, SessionID sessionId) {
        System.out.println("ADMIN OUT - Message: " + message + " for sessionId: " + sessionId);
    }

    @Override
    public void onCreate(SessionID sessionId) {
        session = Session.lookupSession(sessionId);
        System.out.println("Session created. Id: " + sessionId);
    }

    @Override
    public void onLogon(SessionID sessionId) {
        System.out.println("Logged on for sessionId: " + sessionId);
    }

    @Override
    public void onLogout(SessionID sessionId) {
        System.out.println("Logged off for sessionId: " + sessionId);
    }

    public void sendMessage(Message message) {
        if (session != null) {
            session.send(message);
        }
    }

    public void run() {
        var quoteRequest = buildQuoteRequest();
        sendMessage(quoteRequest);
    }

    private QuoteRequest buildQuoteRequest() {
        QuoteRequest quoteRequest = new QuoteRequest(new QuoteReqID("HL."));
        QuoteRequest.NoRelatedSym request = new QuoteRequest.NoRelatedSym();

        DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyyMMdd");
        DateTimeFormatter date1 = DateTimeFormatter.ofPattern("yyyyMMdd-HH:mm:ss");

        request.set(new Symbol("HL."));
        request.set(new OrderQty(555));
        request.set(new SecurityID("GB0008706128"));
        request.set(new SecurityExchange("L"));
        request.set(new Side('1'));
        request.set(new Currency("GBX"));
        request.set(new SettlDate(LocalDateTime.now().format(date)));
        request.set(new SettlType("6"));


        request.setField(new StringField(60, LocalDateTime.now().format(date1)));
        quoteRequest.setField(new StringField(120, "GBX"));
//        request.setField(new StringField(5014, "NET"));

        quoteRequest.addGroup(request);

        return quoteRequest;
    }
}
