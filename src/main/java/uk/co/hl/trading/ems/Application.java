package uk.co.hl.trading.ems;

import quickfix.FileLogFactory;
import quickfix.FileStoreFactory;
import quickfix.SessionSettings;
import quickfix.SocketInitiator;
import quickfix.fix44.MessageFactory;

public class Application {

    public static void main(String[] args) {
        System.out.println("""
                =============
                This is only an example program.
                It's a simple client (e.g. Initiators) that connects to servers (e.g. Acceptors)
                To connecting to the server, it should set TargetCompID to 'CLIENT1' or 'CLIENT2' and SenderCompID to 'SIMPLE'.
                Port is 5001.
                (see simple-initiator.cfg for configuration details)"
                ============="""
        );

        try {
            var settings = new SessionSettings("simple-initiator.cfg");
            var app = new SimpleInitiator();
            var storeFactory = new FileStoreFactory(settings);
            var logFactory = new FileLogFactory(settings);
            var messageFactory = new MessageFactory();
            var initiator = new SocketInitiator(app, storeFactory, settings, logFactory, messageFactory);

            initiator.start();
            Thread.sleep(5000);
            app.run();
            Thread.sleep(5000);
//            initiator.stop();

        } catch (Exception e) {
            System.out.println("==FATAL ERROR==");
            System.out.println(e.getMessage());
        }
    }
}
